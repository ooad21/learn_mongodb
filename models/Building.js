const mongoose = require('mongoose')
const { Schema } = mongoose
const buildind = new Schema({
  name: { type: String },
  floor: Number,
  room: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }]

})
module.exports = mongoose.model('Building', buildind)
